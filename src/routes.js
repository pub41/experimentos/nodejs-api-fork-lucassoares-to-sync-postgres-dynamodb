const routes = require('express').Router();
const User = require('./models/user.js');

routes.get('/', async(req, res)=>{
  const users = await User.scan().exec();
  return res.json({users});
});

routes.get('/:id', async(req, res)=>{
  const user = await User.get(req.params.id);
  return res.json(user);
});

routes.post('/', async(req, res) => {
  let requestArray = req.body.data.split(',')

  const user = await User.create({
    name: requestArray[0],
    email: requestArray[1]
  });

  return res.json(req.body);
});

routes.put('/:id', async(req, res)=>{
  const user = await User.update(req.params.id,req.body);
  return res.json(user);
});

routes.delete('/:id', async(req, res)=>{
  await User.delete(req.params.id);
  return res.json({msg: 'Deleted'});
});

module.exports = routes;
