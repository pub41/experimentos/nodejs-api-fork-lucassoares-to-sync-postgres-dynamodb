const dynamoose = require('dynamoose');
const uuid = require('uuid');

dynamoose.AWS.config.update({
  region: 'us-east-1',
  endpoint: "http://localhost:8000"
});

dynamoose.local();

const UserSchema = new dynamoose.Schema({
  id: {
    type: String,
    hashKey: true,
    default: uuid.v1(),
  },
  name: {
    type: String,
  },
  email:{
    type: String,
    required: true,
  }
},
{
  timestamps: true,
});

module.exports = dynamoose.model('User', UserSchema);
